# SPDX-FileCopyrightText: 2024 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import numpy as np
from internal_momentchi2 import hbe, sw, lpb4

from rcit import ApproxMethod


def compute_p_value_from_covariance(
    covariance_matrix: np.ndarray, quantile_values: float, approx_method: ApproxMethod
) -> float:
    """
    Helper function to compute the p-value based on the test statistic and
    the covariance matrix depending on the chosen approximation
    """
    eig_d = np.linalg.eigvals(covariance_matrix)
    eig_d = eig_d[eig_d > 0]

    approx_to_fun = {"gamma": sw, "hbe": hbe, "lpd4": lpb4}

    assert approx_method in approx_to_fun.keys()

    return 1 - approx_to_fun[approx_method](eig_d, quantile_values)


def normalize(x: np.ndarray) -> np.ndarray:
    """
    Normalizes each column of a matrix
    See: https://github.com/ericstrobl/RCIT/blob/master/R/normalize.R
    """
    assert len(x.shape) == 2

    zero_mean_x = x - np.mean(x, axis=0)
    std_x = np.std(x, axis=0)
    std_x[std_x == 0] = 1

    return zero_mean_x / std_x
