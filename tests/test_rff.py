# SPDX-FileCopyrightText: 2024 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import pytest
from pathlib import Path
import pandas as pd
import numpy as np

from sklearn.metrics.pairwise import rbf_kernel

from rcit.rff import random_fourier_features


TEST_FILE_PATH = Path(__file__).resolve().parent.joinpath("data/test.csv")


@pytest.fixture
def get_dataset():
    dataset = pd.read_csv(TEST_FILE_PATH)
    return dataset


def test_rff_shape(get_dataset):
    num_f = 24
    feats = random_fourier_features(get_dataset["x"], num_f=num_f)["feat"]

    assert feats.shape == (len(get_dataset["x"]), num_f)


def test_convergence_true_rbf(get_dataset):
    """
    Test de la convergence de la matrice approchée de covariance des RFF
    vers la vraie matrice du noyau Gaussien
    """
    num_f = 5 * 10**4
    feats = random_fourier_features(get_dataset["x"], num_f=num_f, sigma=1, seed=42)["feat"]
    approximated_covariance_matrix = 1 / (num_f - 1) * feats @ feats.T
    true_rbf_kernel = rbf_kernel(
        get_dataset["x"].values.reshape(-1, 1), get_dataset["x"].values.reshape(-1, 1), gamma=1 / 2.0
    )
    assert np.mean(np.abs(true_rbf_kernel - approximated_covariance_matrix)) < 1e-2
