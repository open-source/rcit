# SPDX-FileCopyrightText: 2024 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import numpy as np

from rcit.utils import normalize


def test_normalize():
    x = np.array([[1, 2, 3], [3, 2, 0]])

    expected_normalized_x = np.array([[-1, 0, 1], [1, 0, -1]])

    assert abs(np.linalg.norm(normalize(x) - expected_normalized_x)) < 1e-8
