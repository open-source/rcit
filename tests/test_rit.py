# SPDX-FileCopyrightText: 2024 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import pytest
from pathlib import Path
import pandas as pd
import numpy as np

from rcit.independence_tests import RIT, RCoT


TEST_FILE_PATH = Path(__file__).resolve().parent.joinpath("data/test.csv")
ALL_APPROX_METHODS = ["perm", "lpd4", "gamma", "perm"]  # ,"hbe", "chi2"]


@pytest.fixture
def get_dataset():
    dataset = pd.read_csv(TEST_FILE_PATH)
    return dataset


def test_independent_rit(get_dataset):
    """
    R code to reproduce:
    ```R
    library(RCIT)
    data = read.csv(TEST_FILE_PATH)
    s = c()
    for (i in 1:1000){
        s = c(s, RIT(data$x, data$z)$p);
    }
    median(s)
    ```
    The R code gives a value of median(s) in [0.80, 0.81]
    """
    n_experiments = 500
    p_values = [RIT(get_dataset["x"], get_dataset["z"], num_f=5)["p"] for _ in range(n_experiments)]

    assert abs(np.median(p_values) - 0.80) < 2e-2


@pytest.mark.parametrize("approx_method", ALL_APPROX_METHODS)
def test_dependent_rit(approx_method, get_dataset):
    n_experiments = 500
    p_values = [RIT(get_dataset["x"], get_dataset["y"], approx=approx_method)["p"] for _ in range(n_experiments)]

    assert abs(np.median(p_values)) < 1e-2


def test_independent_rcot(get_dataset):
    n_experiments = 500
    p_values = [RCoT(get_dataset["x"], get_dataset["y"], get_dataset["z"])["p"] for _ in range(n_experiments)]

    assert abs(np.median(p_values)) < 1e-2


def test_empty_conditioning_set(get_dataset):
    n_experiments = 500
    p_values = [RCoT(get_dataset["x"], get_dataset["z"])["p"] for _ in range(n_experiments)]

    assert abs(np.median(p_values) - 0.80) < 2e-2
