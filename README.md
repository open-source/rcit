<!--
SPDX-FileCopyrightText: 2024 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>

SPDX-License-Identifier: MIT
-->
Implémentation Python des tests RCIT/RCoT

## Description

Ce dépôt comporte une implémentation Python du package R `rcit` disponible à l'adresse suivante : https://github.com/ericstrobl/RCIT
Il permet de réaliser des tests d'indépendance conditionnelle en grande dimension grâce à une approximation de matrice de noyaux par des 
*random Fourier features*.

L'article original est disponible ici : https://www.degruyter.com/document/doi/10.1515/jci-2018-0017/html?lang=en (Eric Strobl et al.)

Il y a aussi une copie de https://github.com/deanbodenham/momentchi2py/tree/master modifié légèrement pour être compatible avec les nouvelles versions de numpy (qui ont enlevé `numpy.math`)

## Installation

```bash
# Récupération du code avec Git
git clone ${GITLAB_URL}
cd rcit

# Création d'un virtualenv et installation des dépendances requises
python3 -m venv .venv


# Installation des dépendances via poetry.
poetry install
```



## Utilisation

```
from rcit import RCoT

test_results = RCoT(X, Y, Z)
```


## Contribution


Avant de contribuer au dépôt, il est nécessaire d'initialiser les _hooks_ de _pre-commit_ :

```bash
pre-commit install
```




## Licence

Ce projet est sous licence MIT. Une copie intégrale du texte
de la licence se trouve dans le fichier [`LICENSES/MIT.txt`](LICENSES/MIT.txt).
